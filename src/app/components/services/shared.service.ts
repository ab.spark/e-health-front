import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SharedService {

    constructor(private httpClient: HttpClient) {
    }

    getServicesList() {
        this.httpClient.get(`${environment.baseUrl}`);
    }

    getBlogList() {
        this.httpClient.get(`${environment.baseUrl}`);
    }

    getFaQList() {
        this.httpClient.get(`${environment.baseUrl}`);
    }

    getDoctorList() {
        this.httpClient.get(`${environment.baseUrl}`);
    }

    getTestimonialList() {
        this.httpClient.get(`${environment.baseUrl}`);
    }

    subscribeToNewsLetter() {
        this.httpClient.post(`${environment.baseUrl}`, {});
    }

}
